""" This script reads the feature vectors and train the biclass classification models
    using three different algorithms (Random Forest, Artificial Neural Network, and Support Vector Machine).
    The hyperparameters of each model are optimized using Bayesian Optimization (implemented by hyperOpt)
    and the optimized models are saved at the end. """


""" Import Libraries """
from glob import glob
from pathlib import Path
import pandas as pd
import numpy as np
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn import model_selection
from sklearn.metrics import make_scorer, f1_score
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials, space_eval
from sklearn.externals import joblib


""" Set function to optimize model """
def optimize_model(objective, space, MAX_EVALS):
    """
    This function find the best model with a given hyperparameter space and objective function.

    objective: objective function to minimize
    space: hyperparameter space to optimize
    MAX_EVALS: maximum number of the optimization run
    """
    best_model = fmin(fn=objective,
                      space=space,
                      algo=tpe.suggest,
                      max_evals=MAX_EVALS,
                      trials=Trials())
    return best_model

""" Objective functions for each algorithm """
def objective_RF(params):
    """
    Objective function for RandomForest hyperparameter tuning

    params: model hyperparameter set
    """
    clf = RandomForestClassifier(**params, random_state=0)
    cv_temp = model_selection.cross_val_score(clf,
                                              fv_train,
                                              class_train,
                                              cv=10,
                                              scoring=make_scorer(f1_score, average='weighted'))
    loss = 1 - cv_temp.mean()
    return {'loss': loss, 'params': params, 'status': STATUS_OK}

def objective_ANN(params):
    """
    Objective function for Artificial Neural Network hyperparameter tuning

    params: model hyperparameter set
    """
    clf =  MLPClassifier(**params, max_iter=500, random_state=0)
    cv_temp = model_selection.cross_val_score(clf,
                                              fv_train,
                                              class_train,
                                              cv=10,
                                              scoring=make_scorer(f1_score, average='weighted'))
    loss = 1 - cv_temp.mean()
    return {'loss': loss, 'params': params, 'status': STATUS_OK}

def objective_SVM(params):
    """
    Objective function for Support Vector Machine hyperparameter tuning

    params: model hyperparameter set
    """
    clf = SVC(**params, random_state=0)
    cv_temp = model_selection.cross_val_score(clf,
                                              fv_train,
                                              class_train,
                                              cv=10,
                                              scoring=make_scorer(f1_score, average='weighted'))
    loss = 1 - cv_temp.mean()
    return {'loss': loss, 'params': params, 'status': STATUS_OK}

def param_space(model, fvsize):
    """
    Set hyperparameter space for a given model and input data size

    model: ML algorithm
    fvsize: input feature vector size
    """
    if 'RF' in str(model):
        # Parameters space for Random Forest
        space = {
            'max_depth': hp.choice('max_depth', range(1, 50)),
            'max_features': hp.choice('max_features', range(1, fvsize)),
            'n_estimators': hp.choice('n_estimators', range(100, 1000)),
            'criterion': hp.choice('criterion', ['gini', 'entropy']),
            'min_samples_split': hp.loguniform('min_samples_split', np.log(1e-5), np.log(0.5)),
            'min_samples_leaf': hp.loguniform('min_samples_leaf', np.log(1e-5), np.log(0.5))
        }
    elif 'ANN' in str(model):
        # Parameters space for Artificial Neural Network
        space = {
            'hidden_layer_sizes': hp.choice('hidden_layer_sizes', range(2, (fv_train.shape[1] + 3))),
            # 'hidden_layer_sizes': hp.choice('hidden_layer_sizes',
            #                                 tuple(permutations(range(2, (fvsize + 3)), 2))),
            'activation': hp.choice('activation', ['relu',
                                                   'logistic',
                                                   'tanh']),
            'solver': hp.choice('solver', ['sgd', 'adam']),
            'alpha': hp.loguniform('alpha', np.log(1e-4), np.log(0.1)),
            'learning_rate': hp.choice('learning_rate', ['constant', 'adaptive'])
        }
    elif 'SVM' in str(model):
        # Parameters space for Support Vector Machine
        space = {
            'C': hp.loguniform('C', np.log(10 ** (-2)), np.log(1e7)),
            'kernel': hp.choice('svm_kernel', ['rbf']),
            'gamma': hp.loguniform('gamma', np.log(10 ** (-3)), np.log(1e3))
        }
    return space


####################################################################################
verbose = True

""" Load data """
# List filenames
filenames = glob('../data/*psv*') + glob('../data/*tfv*')

# set path to save the optimized models and results
savepth = '../results/test/'
Path(savepth).mkdir(parents=True, exist_ok=True)
Path(savepth + '/models').mkdir(parents=True, exist_ok=True)

# set scoring factor for optimization
scoring = f1_score    # weighted f1-score

# create an empty dataframe to save f-score values
list_fscore = pd.DataFrame(data=[])

# set maximum number of optimization runs
MAX_EVALS = 2

for filename in filenames:
    # Read csv
    df = pd.read_csv(filename)
    df = df.drop(['seq'], axis=1)

    # extract feature vector type to set the savename later
    trfvtype = filename.split('/')[-1].split('.')[0]

    """ Split data into features and classes """
    # extract the feature vector only
    fv_train = df.drop('class', axis=1)

    # extract class (label) only
    class_train = df['class']
    # set class variable type as int
    class_train.values[class_train.values != 'Y'] = 0
    class_train.values[class_train.values == 'Y'] = 1
    class_train = class_train.astype('int')

    if verbose:
        print('feature vectors and classes(labels) were extracted from the file ({})'.format(filename))

    """ Hyperparameter optimization using Bayesian optimization """
    # The objective function is what we are trying to minimize.
    # We use (1 - 'F-score') as the objective function.

    # ================================= Random Forest ================================ #
    if verbose:
        print('optimizing Random Forest...')
    # Find the best model by optimizing hyperparameters
    best_RF_temp = optimize_model(objective_RF,
                                  param_space('RF', fv_train.shape[1]),
                                  MAX_EVALS)
    # Convert parameter space index to actual parameter space
    best_RF_param = space_eval(param_space('RF', fv_train.shape[1]), best_RF_temp)
    best_RF = RandomForestClassifier(**best_RF_param, random_state=0)

    # Evaluate the best model
    best_RF_scores = model_selection.cross_val_score(best_RF,
                                                     fv_train,
                                                     class_train,
                                                     cv=10,
                                                     scoring=make_scorer(scoring, average='weighted'))

    # set save filename
    savename = savepth + 'models/' + trfvtype + '_best_RF.pkl'
    # save the optimized model
    joblib.dump(best_RF, savename)

    # store F-score to the list to summarize the results at the end
    list_fscore = list_fscore.append(pd.DataFrame(index=[trfvtype + '_RF'], data=[best_RF_scores.mean()]))

    if verbose:
        print('done.. \n{} is {}'.format(scoring.__name__, best_RF_scores.mean()))

    # =========================== Artificial Neural Network ========================== #
    if verbose:
        print('optimizing Artificial Neural Network...')
    # The objective function for the ANN using 10 fold cross validation
    # Find the best model by optimizing hyperparameters
    best_ANN_temp = optimize_model(objective_ANN,
                                   param_space('ANN', fv_train.shape[1]),
                                   MAX_EVALS)
    # Convert parameter space index to actual parameter space
    best_ANN_param = space_eval(param_space('ANN', fv_train.shape[1]), best_ANN_temp)
    best_ANN = MLPClassifier(**best_ANN_param, max_iter=500, random_state=0)

    # Evaluate the best model
    best_ANN_scores = model_selection.cross_val_score(best_ANN,
                                                      fv_train,
                                                      class_train,
                                                      cv=10,
                                                      scoring=make_scorer(scoring, average='weighted'))

    # set save filename
    savename = savepth + 'models/' + trfvtype + '_best_ANN.pkl'
    # save the optimized model
    joblib.dump(best_ANN, savename)

    list_fscore = list_fscore.append(pd.DataFrame(index=[trfvtype + '_ANN'], data=[best_ANN_scores.mean()]))

    if verbose:
        print('done.. \n{} is {}'.format(scoring.__name__, best_ANN_scores.mean()))

    # ============================ Support Vector Machine ============================ #
    if verbose:
        print('optimizing Support Vector Machine...')
    # The objective function for the SVM using Binary relevance
    # Find the best model by optimizing hyperparameters
    best_SVM_temp = optimize_model(objective_SVM,
                                   param_space('SVM', fv_train.shape[1]),
                                   MAX_EVALS)
    # Convert parameter space index to actual parameter space
    best_SVM_param = space_eval(param_space('SVM', fv_train.shape[1]), best_SVM_temp)
    best_SVM = SVC(**best_SVM_param, random_state=0)

    # Evaluate the best model
    best_SVM_scores = model_selection.cross_val_score(best_SVM,
                                                      fv_train,
                                                      class_train,
                                                      cv=10,
                                                      scoring=make_scorer(scoring, average='weighted'))

    # set save filename
    savename = savepth + 'models/' + trfvtype + '_best_SVM.pkl'
    # save the optimized model
    joblib.dump(best_SVM, savename)

    list_fscore = list_fscore.append(pd.DataFrame(index=[trfvtype + '_SVM'], data=[best_SVM_scores.mean()]))

    if verbose:
        print('done.. \n{} is {}'.format(scoring.__name__, best_SVM_scores.mean()))

# set savename for cross-validation scores (F-score)
savename = savepth + 'summary_fscore.csv'
# save cross-validation scores (F-score)
list_fscore.to_csv(savename)




