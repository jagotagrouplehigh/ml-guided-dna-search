""" This script loads the trained ML models,
    reads the test sequence set, then makes prediction.
    The prediction probability for each sequence will be saved at the end. """

""" Import Libraries """
from glob import glob
from pathlib import Path
import pandas as pd
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.externals import joblib

verbose = True

def readfv(fvtype, datanames):
    """
    Return proper feature vector and class for training set and feature vector for test set

    fvtype: feature vector type to find input data filename
    datanames: datanames set
    """

    # filename for training set which includes the specified feature vector type
    trainnames = [sub.replace('/testset', '') for sub in datanames]
    fname_tr = [s for s in trainnames if fvtype in s][0]

    # read training set
    df = pd.read_csv(fname_tr)

    """ Split data into features and classes """
    # extract the feature vector only
    fv_train = df.drop(['seq','class'], axis=1)

    # extract class (label) only
    class_train = df['class']
    # set class variable type as int
    class_train.values[class_train.values != 'Y'] = 0
    class_train.values[class_train.values == 'Y'] = 1
    class_train = class_train.astype('int')

    if verbose:
        print('feature vectors and classes(labels) were extracted from the file ({})'.format(fname_tr))

    # filename for testset which includes the specified feature vector type
    fname_ts = [s for s in datanames if fvtype in s][0]
    # read test set
    fv_test = pd.read_csv(fname_ts)
    # extract sequence list only
    seqlist = fv_test['seq']
    # extract the feature vector only
    fv_test = fv_test.drop(['seq'], axis=1)

    if verbose:
        print('feature vectors for testset was extracted from the file ({})'.format(fname_ts))

    return fv_train, class_train, fv_test, seqlist





####################################################################################

def predict_seq(filename, datanames):
    """
    Predict the results using the trained model and return the prediction probabilities and ranking factor

    filename: model filename
    datanames: list of the input data filenames
    """

    # extract the feature vector type and model algorithm from the filename
    tr_detail = filename.split('/')[-1].split('.')[0].split('_best')[0]
    md_detail = filename.split('/')[-1].split('.')[0].split('best_')[1]

    # extract training feature vector/classes and test feature vector
    fv_train, class_train, fv_test, seqlist = readfv(tr_detail, datanames)

    # Read model
    clf = joblib.load(filename)

    # set the SVM model to return prediction probability
    if md_detail == 'SVM':
        clf.probability = True

    # fit the model
    try:
        clf = clf.fit(fv_train, class_train)
    except:
        if md_detail == 'ANN':
            clf.max_fun = 15000
        elif md_detail == 'RF':
            params = clf.get_params()
            del params['ccp_alpha']
            del params['max_samples']
            clf = RandomForestClassifier(**params)
        elif md_detail == 'SVM':
            print('SVM')
            params = clf.get_params()
            del params['break_ties']
            # SVC.break_ties = False
            clf = SVC(**params)
            clf.probability = True

        clf = clf.fit(fv_train, class_train)

    # make prediction for the test set and get the prediction probability
    try:
        pred = clf.predict(fv_test)
        probs = clf.predict_proba(fv_test)
    except:
        if md_detail == 'SVM':
            params = clf.get_params()
            del params['break_ties']
            # SVC.break_ties = False
            clf = SVC(**params)
            clf.probability = True

            clf = clf.fit(fv_train, class_train)
            probs = clf.predict_proba(fv_test)
            pred = clf.predict(fv_test)

    # extract the sequences and prediction results only when the sequences are predicted as positive
    pred_seq = pd.DataFrame(data=[])
    pred_seq['seq'] = seqlist[pred == 1]
    pred_seq['prob'] = probs[:,1][pred == 1]

    # extract the sequences and prediction results only when the sequences are predicted as negative
    pred_seq_N = pd.DataFrame(data=[])
    pred_seq_N['seq'] = seqlist[pred == 0]
    pred_seq_N['prob'] = probs[:, 0][pred == 0]

    # extract all sequences and prediction results
    pred_all = pd.concat([seqlist, fv_test], axis=1)
    pred_all['prob'] = probs[:,1]

    # set path and filename to save the prediction results
    savepth = filename.split('/models')[0] + '/predicted'
    Path(savepth).mkdir(parents=True, exist_ok=True)
    Path(savepth + '_pos').mkdir(parents=True, exist_ok=True)
    Path(savepth + '_neg').mkdir(parents=True, exist_ok=True)

    # save the prediction results
    savename = '{}_pos/{}-{}.csv'.format(savepth, tr_detail, md_detail)
    pred_seq.to_csv(savename, index=False)

    savename = '{}_neg/{}-{}.csv'.format(savepth, tr_detail, md_detail)
    pred_seq_N.to_csv(savename, index=False)

    savename = '{}/{}-{}.csv'.format(savepth, tr_detail, md_detail)
    pred_all.to_csv(savename, index=False)


""" ======================================== main script ======================================== """

# List filenames
filenames = glob('../results/test/models/*best*')
datanames = glob('../data/testset/*psv*') + glob('../data/testset/*tfv*')

for name in filenames:
    predict_seq(name, datanames)





