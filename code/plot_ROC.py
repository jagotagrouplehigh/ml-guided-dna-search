""" This script loads the trained ML models,
    then return the receiver operating characteristic (ROC) curve
    and the area under the curve (AUC) value. """

""" Import Libraries """
from glob import glob
from pathlib import Path
import pandas as pd
import numpy as np
from scipy import interp
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.externals import joblib
from sklearn.model_selection import StratifiedKFold
from matplotlib import pyplot as plt
from sklearn.metrics import make_scorer, f1_score, roc_curve, auc, confusion_matrix


verbose = True

def readfv(fvtype, datanames):
    """
    Return proper feature vector and class for training set and feature vector for test set

    fvtype: feature vector type to find input data filename
    datanames: datanames set
    """
    # filename for training set which includes the specified feature vector type
    fname_tr = [s for s in datanames if fvtype in s][0]

    # read training set
    df = pd.read_csv(fname_tr)

    """ Split data into features and classes """
    # extract the feature vector only
    fv_train = df.drop(['seq', 'class'], axis=1)

    # extract class (label) only
    class_train = df['class']
    # set class variable type as int
    class_train.values[class_train.values != 'Y'] = 0
    class_train.values[class_train.values == 'Y'] = 1
    class_train = class_train.astype('int')

    if verbose:
        print('feature vectors and classes(labels) were extracted from the file ({})'.format(fname_tr))

    return fv_train, class_train



####################################################################################

def plot_ROC(filename, datanames):
    """
    Return the plot of ROC curve and corresponding AUC value

    filename: model filename
    datanames: list of the input data filenames
    """

    # extract the feature vector type and model algorithm from the filename
    tr_detail = filename.split('/')[-1].split('.')[0].split('_best')[0]
    md_detail = filename.split('/')[-1].split('.')[0].split('best_')[1]

    # extract training feature vector/classes and test feature vector
    fv_train, class_train = readfv(tr_detail, datanames)

    # Read model
    clf = joblib.load(filename)

    # set the SVM model to return prediction probability
    if md_detail == 'SVM':
        clf.probability = True

    # fit the model
    try:
        clf = clf.fit(fv_train, class_train)
    except:
        if md_detail == 'ANN':
            clf.max_fun = 15000
        elif md_detail == 'RF':
            params = clf.get_params()
            del params['ccp_alpha']
            del params['max_samples']
            clf = RandomForestClassifier(**params)
        elif md_detail == 'SVM':
            print('SVM')
            params = clf.get_params()
            del params['break_ties']
            # SVC.break_ties = False
            clf = SVC(**params)
            clf.probability = True

        clf = clf.fit(fv_train, class_train)

    """ Run classifier with 10-fold cross-validation and plot ROC curves """
    # set cross-validation
    cv = StratifiedKFold(n_splits=10)

    # change the dtype of input feature vector and class dataframe to numpy array
    fv_train_np = fv_train.to_numpy()
    class_train_np = class_train.to_numpy()

    # initialize true positive rate (tpr), the area under the curve (auc),
    # and the false positive rate (fpr)
    tprs = []
    aucs = []
    mean_fpr = np.linspace(0, 1, 100)

    # set firgure size
    fig = plt.figure(figsize=(8, 5))

    # run classifier with 10-fold cross-validation and calculate the tpr, fpr, and auc
    i = 0
    for train, test in cv.split(fv_train_np, class_train_np):
        # estimate the prediction probability
        probas_ = clf.fit(fv_train_np[train], class_train_np[train]).predict_proba(fv_train_np[test])

        # Compute ROC curve and area the curve (auc)
        fpr, tpr, thresholds = roc_curve(class_train_np[test], probas_[:, 1])
        tprs.append(interp(mean_fpr, fpr, tpr))
        tprs[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs.append(roc_auc)

        # plot roc curve for each subset
        plt.plot(fpr, tpr, lw=1, alpha=0.3,
                 label='%d-fold (AUC = %0.2f)' % (i, roc_auc))

        i = i + 1


    # plot the control line for a random model which predicts a random class or a constant class in all cases
    plt.plot([0, 1], [0, 1], linestyle='--', lw=2, color='k',
             label='random', alpha=.8)

    # calculate the mean of tpr and auc from cross validation reuslts
    mean_tpr = np.mean(tprs, axis=0)
    mean_tpr[-1] = 1.0
    mean_auc = auc(mean_fpr, mean_tpr)

    # plot standard deviation of auc and tpr (optional)
    std_auc = np.std(aucs)
    plt.plot(mean_fpr, mean_tpr, color='r',
             label=r'Mean (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             lw=2, alpha=.8)

    std_tpr = np.std(tprs, axis=0)
    tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
    tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
    plt.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2,
                     label=r'$\pm$ 1 std. dev.')

    plt.gca().set_aspect('equal', adjustable='box')
    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))

    if verbose:
        plt.show()

    # set path and filename to save the plot of ROC curve and AUC value
    savepth = filename.split('/models')[0] + '/roc'
    Path(savepth).mkdir(parents=True, exist_ok=True)
    savename = '{}/roc_{}_{}'.format(savepth, tr_detail, md_detail)

    # save figure and raw data
    fig.savefig('{}.pdf'.format(savename), dpi=300)
    pd.DataFrame(np.column_stack((mean_fpr, mean_tpr))).to_csv('{}.csv'.format(savename), index=False,
                                                               header=['fpr', 'tpr'])

    if verbose:
        print('roc curve for the model {} trained using {} was saved in {}'.format(md_detail, tr_detail, savepth))

""" ======================================== main script ======================================== """

# List filenames
filenames = glob('../results/test/models/*best*')
datanames = glob('../data/*psv*') + glob('../data/*tfv*')


for name in filenames:
    plot_ROC(name, datanames)


